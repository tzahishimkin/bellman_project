from policy_tools.policy_wrapper import PolicyWrapper
from policy_tools.DQN_1D import DQN_1D
from policy_tools.dummy_policy import DummyPolicy, DummyPolicyWrapper


def get_policy_wrapper(policy_name, env, args):
    if policy_name == 'dummy':
        policy_net = DummyPolicy(input=env.state_dim, output=env.action_space.n)
        policy_wrapper = DummyPolicyWrapper(policy_net)
    elif policy_name == 'DQN1':
        policy_net = DQN_1D(input=env.state_dim, output=env.action_space.n)
        policy_wrapper = PolicyWrapper(env=env,
                                       model=policy_net,
                                       checkpoint=args.checkpoint_dir,
                                       save_data_dir=args.save_data_dir,
                                       load_pretrained_model=args.resume_policy
                                       )
    return policy_wrapper