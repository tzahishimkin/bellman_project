from policy_tools.replay_buffer import ReplayBuffer
from project_utils.gen_utils import GenUtils

device = GenUtils.get_device()
GenUtils.set_device_config()

def generate_replay_buffer(env, model, capacity, epsilon=0.4):
    replay_buffer = ReplayBuffer(capacity)
    iter_count = 0

    state = env.reset()
    state.to(device)
    while not replay_buffer.is_full():
        action = model.act(state, epsilon=epsilon)
        next_state, reward, done, _ = env.step(action)
        next_action = model.act(next_state, epsilon=0)
        replay_buffer.push(state, action, reward, next_state, done, next_action)
        if done or iter_count > 20:
            state = env.reset()
            iter_count = 0
        else:
            iter_count += 1
            state = next_state
    return replay_buffer
